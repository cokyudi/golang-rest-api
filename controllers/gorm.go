package controllers

import "github.com/jinzhu/gorm"

//InDB def
type InDB struct {
	DB *gorm.DB
}
