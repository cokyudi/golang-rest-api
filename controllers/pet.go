package controllers

import (
	"net/http"

	"../structs"
	"github.com/gin-gonic/gin"
)

//GetPet Method
func (idb *InDB) GetPet(c *gin.Context) {
	var (
		pet    structs.Pet
		result gin.H
	)
	id := c.Param("id")
	err := idb.DB.Joins("join people on people.id=pets.person_id").Where("pets.id=?", id).Find(&pet).Error
	if err != nil {
		result = gin.H{
			"result": err.Error(),
			"count":  0,
		}
	} else {
		result = gin.H{
			"result": idb.DB.Model(&pet).Preload("Person").Find(&pet),
			"count":  1,
		}
	}

	c.JSON(http.StatusOK, result)
}
