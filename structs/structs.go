package structs

import "github.com/jinzhu/gorm"

//Person table
type Person struct {
	gorm.Model
	First_Name string
	Last_Name  string
}

type Pet struct {
	gorm.Model
	Pet_Name  string
	Pet_kind  string
	Person_id int
	Person    Person `gorm:"foreignKey:Person_id;association_foreignKey:ID"`
}
